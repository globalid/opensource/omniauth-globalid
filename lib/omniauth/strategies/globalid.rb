# frozen_string_literal: true

require "omniauth-oauth2"
require "jwt"

module OmniAuth
  module Strategies
    class Globalid < OmniAuth::Strategies::OAuth2
      option :name, "globalid"
      DEFAULT_SCOPE = "public"

      option :client_options, site: "https://auth.global.id",
                              authorize_url: "/",
                              token_url: "https://api.globalid.net/v1/auth/token"

      def self.parse_jwt(id_token)
        JWT.decode(id_token, nil, false).first
      end

      # https://github.com/omniauth/omniauth-oauth2/issues/81
      def callback_url
        full_host + script_name + callback_path
      end

      def authorize_params
        auth_params = super # Get the OAuth2 omniauth params
        # Add the acrc_id if configured
        auth_params[:acrc_id] = options[:acrc_id] if options[:acrc_id]
        # If we are getting pii sharing, we need to have the openid scope
        if pii_sharing?
          auth_params[:scope] = "openid"
        end
        # If we are in the openid scope, we need a nonce
        if options[:scope]&.match?("openid")
          auth_params[:nonce] ||= SecureRandom.hex(24)
        end
        return auth_params unless acrc_id_in_request?
        auth_params.merge(acrc_id: request.params["acrc_id"] || request.params[:acrc_id])
      end

      uid { raw_info["gid_uuid"] }

      info do
        {
          nickname: nickname(raw_info),
          name: raw_info["display_name"],
          description: raw_info["description"],
          image: raw_info["display_image_url"],
          location: location(raw_info),
        }.merge(id_token: openid_token)
         .merge(decrypted_pii: decrypted_pii)
      end

      def raw_info
        return @raw_info if defined?(@raw_info)

        result = api_connection.get("/v1/identities/me")
        @raw_info = JSON.parse(result.body)
      end

      def openid_token
        return @openid_token if defined?(@openid_token)
        id_token = access_token["id_token"]
        if !id_token
          @openid_token = {}
        else
          @openid_token = self.class.parse_jwt(id_token)
        end
        @openid_token
      end

      def decrypted_pii
        return {} unless openid_token.keys.any? && options[:decrypt_pii_on_login]
        @decrypted_pii ||= vault.decrypted_pii
      end

      private

      def api_connection
        Faraday.new(url: "https://api.globalid.net") do |conn|
          conn.headers["Authorization"] = "Bearer #{access_token.token}"
          conn.headers["Content-Type"] = "application/json"
          conn.adapter Faraday.default_adapter
        end
      end

      def vault
        OmniAuth::Globalid::Vault.new(openid_token: openid_token,
                                      token_url: options[:token_url],
                                      client_id: options[:client_id],
                                      client_secret: options[:client_secret],
                                      redirect_uri: options[:redirect_uri],
                                      private_key: options[:private_key],
                                      private_key_pass: options[:private_key_pass])
      end

      def acrc_id_in_request?
        request.params.key?("acrc_id") || request.params[:acrc_id]
      end

      def acrc_id_provided?
        options[:acrc_id] || acrc_id_in_request?
      end

      def pii_sharing?
        # TODO: make this actually check if we need PII sharing. For now, just assuming
        acrc_id_provided? && options.key?("private_key")
      end

      def location(raw_info)
        location = [
          raw_info["metro_name"],
          raw_info["state_name"],
          raw_info["country_code"],
        ].compact.map(&:strip).reject(&:empty?)

        return if location.empty?

        location.join(", ")
      end

      def nickname(raw_info)
        return if raw_info["gid_name_moderation_status"] != "accepted"

        raw_info["gid_name"]
      end
    end
  end
end
