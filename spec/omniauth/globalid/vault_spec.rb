require "spec_helper"
require "dotenv/load" # Because we need to load the variables

describe OmniAuth::Globalid::Vault do
  let(:access_token) { JSON.parse(File.read("spec/fixtures/openid_connect_access_token.json")) }
  let(:openid_token) { OmniAuth::Strategies::Globalid.parse_jwt(access_token["id_token"]) }
  let(:strategy) { OmniAuth::Strategies::Globalid.new("appid", "secret", @options || {}) }
  let(:subject) { OmniAuth::Globalid::Vault.new(openid_token: openid_token, private_key: ENV["GLOBALID_PRIVATE_KEY"], private_key_pass: ENV["GLOBALID_PRIVATE_KEY_PASS"]) }

  describe "encrypted_data_tokens" do
    let(:target) do
      ["WQexnTFKt1EFbqTi60fjGTCV67lCZMxpjuw6Euv9VvIxAksVWOG+rcXddAhg9RyqnD5663wPaCjvQxZPGKjisz3Xcrll8BTzeSfEGY9GMM2pcXutmSZKDFuLgUXzDJqD9sl0ekWdVTb9SdnAVeAYkztmAYpf7E9KJTta5HyXEwXPKsI/L17MF5vuFGxfYL84P1tbCeB67pYtl4Cp1vVpg53oVu/wvWKWVOLgwDnRgkOa5gvTsROoxRqOhMoWaMLsTYREOUwfs1rv1mVKOtXXq9IkK6kZ6DfC5PgYKOCI3OLaOvNcWy8qd+zdGZBRXeyHDIFrotY1omAMTTBHmFNMwOQddTIWNVgKaPc4xV5q2NfCA6qdEREiSIysTPeWVgd134UvVNjCVTeTIb365hgB0Mj/tlMww8B1BqWPFmxYzFkyQkcUvNdJGqcwUpjjpQI/OxzzunhMLNZoxK1/PeSPRxDbo9Ii8QeJy2suQNqdSsCEqvKYHMN7WoLeyQaTs6GodIbL9KiIbsJ+LEAOvZwk7Z9h/eLFsGVzVv/ubRblWSpUQlJ+F/uU+FuFULJXwQJ3vAhwppxFzcElncrI5PeUqqXUVdC6w3A5b8qzVEmT172xNEOEjnsWZqls4uAJz7qIu/7xBRSPlX+fTD1GZaHhvTVfPESMeFvZYfTk1V10PJc="]
    end
    it "gets the key values" do
      expect(subject.encrypted_data_tokens).to eq target
    end
  end

  describe "decrypted_tokens" do
    let(:target) { ["5ac8673893354bbf8ec6df48f137dcbf"] }
    it "is the tokens we expected" do
      expect(subject.decrypted_tokens).to eq target
    end
  end

  describe "client_credentials_access_token" do
    let(:target) { "ddd" }
    xit "returns the access_token we expect" do
      # Don't know how to block webmock, will resolve in the future
      expect(subject.client_credentials_access_token).to eq target
    end
  end

  describe "vault_response" do
    let(:target) { [] }
    xit "returns the vault response" do
      # Don't know how to block webmock, will resolve in the future
      expect(subject.vault_response).to eq target
    end
  end

  describe "decrypted_pii" do
    let(:target) { ["sdfsdf"] }

    xit "decrypts the things" do
      # Don't know how to block webmock, will resolve in the future
    end
  end
end
